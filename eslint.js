module.exports = {
  extends: 'eslint:recommended',
  rules: {
    'array-bracket-spacing': [2, 'never'],
    'array-callback-return': 2,
    'arrow-parens': ['error', 'as-needed', {
      requireForBlockBody: true
    }],
    'arrow-spacing': 2,
    'block-scoped-var': 2,
    'block-spacing': [2, 'always'],
    'brace-style': [2, '1tbs', {
      allowSingleLine: true
    }],
    'camelcase': [2, {
      properties: 'never'
    }],
    'class-methods-use-this': 1,
    'comma-dangle': ['error', {
      arrays: 'always-multiline',
      objects: 'always-multiline',
      imports: 'always-multiline',
      exports: 'always-multiline',
      functions: 'never'
    }],
    'comma-spacing': [2, {
      before: false,
      after: true
    }],
    'comma-style': [2, 'last'],
    'computed-property-spacing': [2, 'never'],
    'consistent-return': 2,
    'consistent-this': 2,
    'curly': [2, 'multi-line', 'consistent'],
    'dot-location': [2, 'property'],
    'dot-notation': 2,
    'eol-last': 2,
    'eqeqeq': [2, 'allow-null'],
    'func-call-spacing': [2, 'never'],
    'func-name-matching': [2, 'always'],
    'func-style': [2, 'declaration', {
      allowArrowFunctions: true
    }],
    'generator-star-spacing': [2, 'after'],
    'handle-callback-err': 2,
    'id-length': [2, {
      min: 2,
      max: 64,
      properties: 'never',
      exceptions: ['i', 'j', 'k', 'x', '_', '$']
    }],
    'indent': [2, 2, {
      SwitchCase: 1,
      MemberExpression: 1
    }],
    'jsx-quotes': [2, 'prefer-single'],
    'key-spacing': [2, {
      beforeColon: false,
      afterColon: true
    }],
    'keyword-spacing': [2, {
      before: true,
      after: true
    }],
    'lines-around-comment': [2, {
      beforeBlockComment: true,
      allowObjectStart: true,
      allowArrayStart: true,
      allowBlockStart: true
    }],
    'new-cap': [2, {
      properties: false
    }],
    'new-parens': 2,
    'no-array-constructor': 2,
    'no-class-assign': 2,
    'no-console': 0,
    'no-const-assign': 2,
    'no-constant-condition': [2, {
      checkLoops: false
    }],
    'no-delete-var': 2,
    'no-dupe-args': 2,
    'no-dupe-class-members': 2,
    'no-dupe-keys': 2,
    'no-duplicate-imports': 2,
    'no-else-return': 1,
    'no-empty': [2, {
      allowEmptyCatch: true
    }],
    'no-empty-character-class': 2,
    'no-empty-pattern': 2,
    'no-eval': 2,
    'no-extend-native': 2,
    'no-extra-bind': 2,
    'no-extra-label': 2,
    'no-extra-parens': [2, 'all', {
      ignoreJSX: 'all',
      conditionalAssign: false,
      returnAssign: false,
      nestedBinaryExpressions: false
    }],
    'no-implicit-globals': 2,
    'no-implied-eval': 2,
    'no-label-var': 2,
    'no-lone-blocks': 2,
    'no-lonely-if': 2,
    'no-loop-func': 2,
    'no-mixed-operators': 2,
    'no-mixed-spaces-and-tabs': 2,
    'no-multi-spaces': 2,
    'no-multi-str': 2,
    'no-multiple-empty-lines': [2, {
      max: 1
    }],
    'no-native-reassign': 2,
    'no-nested-ternary': 2,
    'no-new': 2,
    'no-new-func': 2,
    'no-new-object': 2,
    'no-new-symbol': 2,
    'no-new-wrappers': 2,
    'no-path-concat': 2,
    'no-proto': 2,
    'no-redeclare': 2,
    'no-return-assign': 2,
    'no-self-compare': 2,
    'no-sequences': 2,
    'no-shadow-restricted-names': 2,
    'no-this-before-super': 2,
    'no-throw-literal': 2,
    'no-trailing-spaces': 2,
    'no-undef': 2,
    'no-undef-init': 2,
    'no-undefined': 2,
    'no-unmodified-loop-condition': 2,
    'no-unneeded-ternary': 2,
    'no-unsafe-negation': 2,
    'no-unused-expressions': [2, {
      allowShortCircuit: true,
      allowTernary: true
    }],
    'no-unused-vars': [2, {
      vars: 'all',
      args: 'none'
    }],
    'no-use-before-define': [2, 'nofunc'],
    'no-useless-call': 2,
    'no-useless-computed-key': 2,
    'no-useless-concat': 2,
    'no-useless-constructor': 2,
    'no-useless-escape': 2,
    'no-useless-rename': 2,
    'no-useless-return': 2,
    'no-var': 2,
    'no-void': 2,
    'no-whitespace-before-property': 2,
    'no-with': 2,
    'object-curly-spacing': [2, 'always'],
    'object-shorthand': 2,
    'one-var': [2, {
      uninitialized: 'always',
      initialized: 'never'
    }],
    'operator-linebreak': [2, 'before'],
    'prefer-arrow-callback': [2, {
      allowNamedFunctions: true
    }],
    'prefer-const': 1,
    'prefer-promise-reject-errors': 2,
    'prefer-template': 1,
    'quote-props': [2, 'consistent-as-needed'],
    'quotes': [2, 'single'],
    'radix': 2,
    'require-yield': 0,
    'rest-spread-spacing': [2, 'never'],
    'semi': [2, 'always'],
    'semi-spacing': [2, {
      before: false,
      after: true
    }],
    'sort-imports': 2,
    'spaced-comment': [2, 'always', {
      line: {
        markers: ['/'],
        exceptions: ['-', '+']
      },
      block: {
        markers: ['!'],
        exceptions: ['*'],
        balanced: true
      }
    }],
    'space-before-blocks': [2, 'always'],
    'space-before-function-paren': [2, {
      anonymous: "never",
      named: "never",
      asyncArrow: "always"
    }],
    'space-in-parens': [2, 'never'],
    'space-infix-ops': 2,
    'space-unary-ops': [2, {
      words: false,
      nonwords: false
    }],
    'strict': [2, 'global'],
    'template-curly-spacing': [2, 'never'],
    'template-tag-spacing': [2, 'never'],
    'unicode-bom': [2, 'never'],
    'valid-jsdoc': 1,
    'wrap-iife': 2,
    'yield-star-spacing': [2, 'after'],
    'yoda': [2, 'never']
  },
  parserOptions: {
    ecmaVersion: 7,
  },
  env: {
    node: true,
    es6: true,
  }
};
